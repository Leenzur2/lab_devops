'use strict';

const express = require('express');

// Constants
const PORT = 8081;
const HOST = '0.0.0.0';

let tests = [];

// App
const app = express();
app.get('/tests', (req, res) => {
  res.send(JSON.stringify(tests));
});
app.get('/test/new', (req, res) => {
  let test = { id: tests.length, name: 'test' + tests.length };
  tests.push(test)
  res.send('Ajouté : ' + JSON.stringify(test));
});
app.get('/test/clear', (req, res) => {
  tests = [];
  res.send('Réinitialisé !');
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT} c`);
